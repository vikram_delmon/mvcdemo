﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Helper;

namespace MVCDemo.Entity
{
    [Table("Customer", Schema = "dbo")]
    public class Customer : BaseEntity
    {
        [Column("Name")]          
        public System.String Name { get; set; }

        [Column("Address")]
        public System.String Address { get; set; }

        [Column("Contact_Person")]
        public System.String ContactPerson { get; set; }

        [Column("Contact_Number")]        
        public System.String ContactNumber { get; set; }

        [Column("Email")]
        public System.String Email { get; set; }

        [Column("GST")]
        public System.String GST { get; set; }

        [Column("No_Of_Locations")]
        public System.Int32 NoOfLocations { get; set; }

        [Column("No_Of_Inventory_Items")]
        public System.Int32 NoOfInventoryItems { get; set; }
    }
}
