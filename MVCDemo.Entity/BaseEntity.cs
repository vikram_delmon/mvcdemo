﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Helper;

namespace MVCDemo.Entity
{
    public class BaseEntity
    {
        #region [Fields]
        private readonly System.DateTime _dateTime = System.DateTime.Now;
        #endregion

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public virtual System.Int64 ID { get; set; }

        [Column("Created_By")]        
        public System.String CreatedBy { get; set; }

        [Column("Created_On")]  
        public System.DateTime CreatedOn { get; set; }

        [Column("Modified_By")]        
        public System.String ModifiedBy { get; set; }

        [Column("Modified_On")]                
        public System.DateTime ModifiedOn { get; set; }

        [Column("Is_Active")]
        public System.Boolean IsActive { get; set; }

        [Column("Is_Deleted")]        
        public System.Boolean IsDeleted { get; set; }

        [Column("Update_Seq")]
        public System.Int32 UpdateSeq { get; set; }

    }
}
