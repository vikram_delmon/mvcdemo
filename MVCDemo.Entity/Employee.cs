﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Entity
{
    public class Employee : Person
    {

        [Column("Gender_ID")]
        public int GenderId { get; set; }

        [ForeignKey("Gender_ID")]
        public virtual Code Gender { get; set; }

        [Column("Gender_Value")]
        public string GenderValue { get; set; }

        [Column("Marital_Status_ID")]
        public int MaritalStatusId { get; set; }

        [ForeignKey("Marital_Status_ID")]
        public virtual Code MaritalStatus { get; set; }

        [Column("Marital_Status_Value")]
        public string MaritalStatusValue { get; set; }

        [Column("Personal_Email")]
        public string PersonalEmail { get; set; }

        [Column("Personal_Contact_1")]
        public string PersonalContact1 { get; set; }

        [Column("Personal_Contact_2")]
        public string PersonalContact2 { get; set; }

        [Column("Emergency_Contact_Name")]
        public string EmergencyContactName { get; set; }

        [Column("Emergency_Contact_Number")]
        public string EmergencyContactNumber { get; set; }

        [Column("Photo")]
        public string Photo { get; set; }     

       
    }
}
