﻿using MVCDemo.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Entity
{
    [Table("Course", Schema = "dbo")]
    public class Course : BaseEntity
    {
        [Column("Course_Code")]
        public string CourseCode { get; set; }

        [Column("Title")]
        public string Title { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
        
    }


}
