﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Helper;

namespace MVCDemo.Entity
{
    [Table("Category", Schema = "dbo")]
    public class Category : BaseEntity
    {
        [Column("Name")]
        public System.String Name { get; set; }


        public virtual Customer Customer { get; set; }
    }
}
