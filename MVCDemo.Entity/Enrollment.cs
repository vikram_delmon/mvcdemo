﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Entity
{
    

    [Table("Enrollment", Schema = "dbo")]
    public class Enrollment : BaseEntity
    {

        [Column("Course_ID")]
        [Display(Name = "Course ID")]
        public long CourseID { get; set; }

        [Column("Student_ID")]
        [Display(Name = "Student ID")]
        public long StudentID { get; set; }

        [Column("Grade_ID")]
        [Display(Name = "Grade ID")]
        public long GradeID { get; set; }

        [ForeignKey("CourseID")]
        public virtual Course Course { get; set; }

        [ForeignKey("StudentID")]
        public virtual Customer Student { get; set; }

        [ForeignKey("GradeID")]
        public virtual Grade Grade { get; set; }
    }
}
