﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Helper;

namespace MVCDemo.Entity
{
    public abstract class Person : BaseEntity
    {
        [Column("Last_Name")]
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = busConstant.Messages.Type.Validations.Length2to50)]
        public System.String LastName { get; set; }

        [Column("First_Name")]
        [Display(Name = "First Name")]
        [Required(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = busConstant.Messages.Type.Validations.Length2to50)]
        public System.String FirstName { get; set; }

        [Column("Middle_Name")]
        [Display(Name = "Middle Name")]
        [Required(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = busConstant.Messages.Type.Validations.Length2to50)]
        public System.String MiddleName { get; set; }

        [Column("Date_Of_Birth", TypeName = "Date")]
        [Display(Name = "Date Of Birth")]
        [Required(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        [DataType(DataType.Date)]
        public System.DateTime DateOfBirth { get; set; }

        [NotMapped]
        [Display(Name = "Age In Year And Month")]
        public System.String AgeInYearAndMonth => busGlobalFunction.CalculateAgeInYearAndMonths(this.DateOfBirth, System.DateTime.Now.Date);

        [NotMapped]
        [Display(Name = "Age In Year")]
        public System.Int32 AgeInYear => busGlobalFunction.AgeInYears(this.DateOfBirth);


        [NotMapped()]
        [Display(Name = "Full Name")]
        public System.String FullName => ($"{this.FirstName} {this.MiddleName} {this.LastName}");




    }
}
