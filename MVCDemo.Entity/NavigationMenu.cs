﻿using MVCDemo.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Entity
{
    public class NavigationMenu : BaseEntity
    {

        public System.String MenuID { get; set; }
        public System.String MenuName { get; set; }
        public System.String Parent_MenuID { get; set; }
        public System.String UserRoll { get; set; }
        public System.String MenuFileName { get; set; }
        public System.String MenuURL { get; set; }

        [DefaultValue(busConstant.Misc.FLAG_YES)]
        public System.String USEYN { get; set; }
    }
}
