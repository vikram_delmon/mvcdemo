﻿using MVCDemo.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Entity
{
    [Table("Grade", Schema = "dbo")]
    public class Grade : BaseEntity
    {

        [Column("Grade_Name")]
        [Display(Name = "Grade Name")]
        [Required(ErrorMessage = busConstant.Messages.Type.Validations.Required)]        
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [StringLength(2, MinimumLength = 1, ErrorMessage = busConstant.Messages.Type.Validations.Length1to2)]
        public string GradeName { get; set; }

        [Column("Section")]
        [Display(Name = "Section")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        public string Section { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
