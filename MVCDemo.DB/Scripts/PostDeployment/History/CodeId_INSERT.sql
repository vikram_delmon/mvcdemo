﻿
IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='Country')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1001,'Country','Country Name',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='State')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1002,'State','State Name',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='District')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1003,'District','District Name',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='Taluka')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1005,'Taluka','Taluka Name',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='City')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1004,'City','City Name',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='Village')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1006,'Village','Village Name',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='PostOffice')
BEGIN	
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1007,'PostOffice','Post Office Value',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='NamePrefix')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1008,'NamePrefix','Name Prefix',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='NameSuffix')
BEGIN
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1009,'NameSuffix','Name Suffix',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='Gender')
BEGIN	
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1010,'Gender','Gender',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='MarritalStatus')
BEGIN	
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1011,'MarritalStatus','Marrital Status',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='CropCategory')
BEGIN	
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1012,'CropCategory','Crop Category',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='CropClassification')
BEGIN	
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1013,'CropClassification','Crop Classification',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='CropType')
BEGIN	
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1014,'CropType','Crop Type',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO


IF NOT EXISTS(SELECT 1 FROM dbo.Code WITH(NOLOCK) WHERE Data1Caption='PlotLandType')
BEGIN	
	INSERT INTO dbo.Code(CodeId,Data1Caption,[Description],Data1Type,Data2Caption,Data2Type,Data3Caption,Data3Type,Comments,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,UpdateSeq)
	VALUES(1015,'PlotLandType','Plot Land Type',NULL,NULL,NULL,NULL,NULL,NULL,'dbo',GETDATE(),'dbo',GETDATE(),0)
END
GO
