﻿

DECLARE @UserID INT, @RoleID INT
SELECT @UserID=Id FROM [dbo].[AspNetUsers] WITH(NOLOCK) WHERE UserName='KMDEV001'
SELECT @RoleID=Id FROM [dbo].[AspNetRoles] WITH(NOLOCK) WHERE [Name]='DEVLP'
IF NOT EXISTS(SELECT 1 FROM [dbo].[AspNetUserRoles] WITH(NOLOCK) WHERE [UserId]=@UserID AND [RoleId]=@RoleID)
BEGIN
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (@UserID, @RoleID)
INSERT [dbo].[AppUser] ([AspNetUserId]) VALUES (@UserID)
END
GO

DECLARE @UserID INT, @RoleID INT
SELECT @UserID=Id FROM [dbo].[AspNetUsers] WITH(NOLOCK) WHERE UserName='KMDEV002'
SELECT @RoleID=Id FROM [dbo].[AspNetRoles] WITH(NOLOCK) WHERE [Name]='DEVLP'
IF NOT EXISTS(SELECT 1 FROM [dbo].[AspNetUserRoles] WITH(NOLOCK) WHERE [UserId]=@UserID AND [RoleId]=@RoleID)
BEGIN
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (@UserID, @RoleID)
INSERT [dbo].[AppUser] ([AspNetUserId]) VALUES (@UserID)
END
GO

DECLARE @UserID INT, @RoleID INT
SELECT @UserID=Id FROM [dbo].[AspNetUsers] WITH(NOLOCK) WHERE UserName='KMADMIN'
SELECT @RoleID=Id FROM [dbo].[AspNetRoles] WITH(NOLOCK) WHERE [Name]='ADMIN'
IF NOT EXISTS(SELECT 1 FROM [dbo].[AspNetUserRoles] WITH(NOLOCK) WHERE [UserId]=@UserID AND [RoleId]=@RoleID)
BEGIN
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (@UserID, @RoleID)
INSERT [dbo].[AppUser] ([AspNetUserId]) VALUES (@UserID)
END
GO