﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r .\History\CodeId_INSERT.sql
go

:r .\History\CodeValue_INSERT.sql
go

:r .\History\AspNetRoles_INSERT.sql
go

:r .\History\AspNetUsers_INSERT.sql
go

:r .\History\AspNetUserRoles_INSERT.sql
go