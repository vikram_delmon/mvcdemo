﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Helper
{
    public class JsonValidationError
    {
        public string Key { get; set; }
        public string Message { get; set; }
    }
}
