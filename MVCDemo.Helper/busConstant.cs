﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Helper
{
    public static class busConstant
    {
        public abstract class Settings
        {
            public abstract class DataBase
            {
                public abstract class Postgresql
                {
                    public abstract class Connections
                    {
                        public abstract class ConnectionString
                        {
                            public const string Default = Development;                            
                            //public const string Default = Testing;
                            //public const string Default = Production;
                        }

                        private const string Development = "Development";                        
                        private const string Testing = "TestinMSSQL";
                        private const string Production = "ProductionMSSQL";
                    }
                }

                public abstract class SqlServer
                {
                    public abstract class Connections
                    {
                        public abstract class ConnectionString
                        {
                            public const string Default = Development;
                            //public const string Default = Testing;
                            //public const string Default = Production;
                        }
                        private const string Development = "Development";
                        private const string DevelopmentMSSQL = "DevelopmentMSSQL";
                        private const string Testing = "TestinMSSQL";
                        private const string Production = "ProductionMSSQL";
                    }
                }

            }

            public abstract class Cache
            {
                public abstract class Key
                {
                    public const string CodeTable = "CODE_TABLE";
                    public const string CodeValueTable = "CODE_VALUE_TABLE";
                    public const string NavigationMenuTable = "NAVIGATION_MENU_TABLE";
                }
            }
        }

        public abstract class Messages
        {
            public abstract class Type
            {
                public const string Validation = "Validation";
                public const string Response = "Response";
                public const string Exception = "Exception";

                public abstract class Validations
                {
                    public const string Required = "The {0} is required.";
                    public const string OnlyCharaters = "The digits and special symbols and spaces are not allowed in {0}.";
                    public const string NoSpace = "The spaces between characters are not allowed in {0}.";

                    public const string Length = "The length of {0} must be between {1} and {2} characters.";

                    public const string Length1to2 = "Data length must be between 1 and 2 characters.";
                    public const string Length2to4 = "Data length must be between 2 and 4 characters.";
                    public const string Length2to50 = "Data length must be between 2 and 50 characters.";
                }
                public abstract class Responses
                {
                    public const string Save = "Record saved successfully.";
                    public const string Delete = "Record deleted successfully.";
                }

                public abstract class Exceptions
                {
                    public const string SomeThingWentWrong = "Something went wrong.";
                    public const string InternalServerError = "Internal Server Error.";
                }
            }

           
        }

        public abstract class Misc
        {
            public const int MONTHS_IN_YEAR = 12;
            public const int FISCAL_YEAR_START_MONTH = 4;
            public const int FISCAL_YEAR_START_DATE = 1;
            public const int FISCAL_YEAR_END_MONTH = 3;
            public const int FISCAL_YEAR_END_DATE = 31;

            public const string FLAG_YES = "Y";
            public const string FLAG_NO = "N";
            public const string FLAG_YES_VALUE = "YES";
            public const string FLAG_NO_VALUE = "NO";

            public const bool TRUE = true;
            public const bool FALSE = false;

            public const string SYSTEM = "SYSTEM";
        }

        public abstract class Forms
        {
            public const string LookUp = "Lookup";
            public const string Maintenance = "Maintenance";
        }

        public abstract class Numbers
        {
            public abstract class Integer
            {
                public const int Zero = 0;
                public const int One = 1;
                public const int Two = 2;
                public const int Three = 3;
                public const int Four = 4;
                public const int Five = 5;
                public const int Six = 6;
                public const int Seven = 7;
                public const int Eight = 8;
                public const int Nine = 9;
                public const int Ten = 10;
            }

            public abstract class Decimal
            {
                public const decimal Zero = 0.0m;
                public const decimal One = 1.0m;
                public const decimal Two = 2.0m;
                public const decimal Three = 3.0m;
                public const decimal Four = 4.0m;
                public const decimal Five = 5.0m;
                public const decimal Six = 6.0m;
                public const decimal Seven = 7.0m;
                public const decimal Eight = 8.0m;
                public const decimal Nine = 9.0m;
                public const decimal Ten = 10.0m;
            }

        }
    }
}
