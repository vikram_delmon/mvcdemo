﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Dapper;


namespace MVCDemo.Helper
{
    [System.ComponentModel.DataObject]
    public class busCache
    {
        public static void Load()
        {

            /* HttpRuntime.Cache.Insert(
          /* key * /                "key",
          /* value * /              suppliers,
          /* dependencies * /       null,
          /* absoluteExpiration * / Cache.NoAbsoluteExpiration,
          /* slidingExpiration * /  Cache.NoSlidingExpiration,
          /* priority * /           CacheItemPriority.NotRemovable,
          /* onRemoveCallback * /   null);*/

            //var Codes = Code.LoadCodeTable();
            //HttpRuntime.Cache.Insert(busConstant.Settings.Cache.Key.CodeTable, Codes);

            //var CodeValues = CodeValues.LoadCodeValueTable();
            //HttpRuntime.Cache.Insert(busConstant.Settings.Cache.Key.CodeValueTable, CodeValues);
            
        }

        //[DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        //public static Northwind.SuppliersDataTable GetSuppliers()
        //{
        //    return HttpRuntime.Cache["key"] as Northwind.SuppliersDataTable;
        //}

        //public static List<KisanMalls.Models.Calendar.Event> CaledarEventTable()
        //{
        //    return HttpRuntime.Cache[knfConstant.CACHE_KEY.CALENDAR_EVENT] as List<KisanMalls.Models.Calendar.Event>;
        //}

        public static IEnumerable<object> CodeTable()
        {
            return HttpRuntime.Cache[busConstant.Settings.Cache.Key.CodeTable] as IEnumerable<object>;
        }

        public static IEnumerable<object> CodeValueTable()
        {
            dynamic cacheData = HttpRuntime.Cache.Get(busConstant.Settings.Cache.Key.CodeValueTable);
            return cacheData.Result as IEnumerable<object>;
        }

        //public static string GetDescription(int CodeId, string CodeValue, string Data1 = "")
        //{
        //    var str = String.Empty;
        //    str = Data1.IsNullOrEmpty() ?
        //         CodeValueTable().Where(x => x.CodeId == CodeId && x.CodeValue == CodeValue).FirstOrDefault().Description :
        //             CodeValueTable().Where(x => x.CodeId == CodeId && x.CodeValue == CodeValue && x.Data1 == Data1).FirstOrDefault().Description;

        //    return str;
        //}
    }
}