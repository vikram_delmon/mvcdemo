﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDemo.Helper
{
    public class QueryResult
    {
        public System.Int64 Id { get; set; }
        public System.Boolean IsSucceed { get; set; }
        public System.String Note { get; set; }
    }
}