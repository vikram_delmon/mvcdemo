﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.Helper
{
    public class JsonResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string ReturnUrl { get; set; }
        public dynamic Entity { get; set; }
        public IEnumerable<string> ValidationSummary { get; set; }
        public IEnumerable<JsonValidationError> Errors { get; set; }

        public JsonResponse()
        {
            Errors = new List<JsonValidationError>();
            ValidationSummary = new List<string>();
        }
    }
}
