﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using MVCDemo.Helper;

namespace MVCDemo.Helper
{
    public class RequiredField : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.IsNotNull())
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(String.Format(ErrorMessage, validationContext.DisplayName));
            }
        }
    }

    public class DataLength : ValidationAttribute
    {
        private int _MaximumLength;
        private int _MinimumLength;

        public DataLength(int MaximumLength, int MinimumLength)
        {
            this._MaximumLength = MaximumLength;
            this._MinimumLength = MinimumLength;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.IsNull())
            {
                return ValidationResult.Success;
            }
            else
            {
                string content = Convert.ToString(value);

                if (content.Length < this._MinimumLength || content.Length > this._MaximumLength)
                {
                    return new ValidationResult(String.Format(ErrorMessage, validationContext.DisplayName, this._MinimumLength, this._MaximumLength));
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
        }



    }
}