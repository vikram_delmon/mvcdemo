﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCDemo.ViewModel
{
    public sealed class CourseIndexViewModel : BaseViewModel
    {

        [Display(Name = "Course Code")]
        public string CourseCode { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }
       
    }
}