﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MVCDemo.Helper;

namespace MVCDemo.ViewModel
{
    public class CourseCreateViewModel : BaseViewModel
    {

        [Display(Name = "Course Code")]
        [RequiredField(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        //[RegularExpression(@"([a-zA-Z]+) (\d+)", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [DataLength(MaximumLength: 4, MinimumLength: 2, ErrorMessage = busConstant.Messages.Type.Validations.Length)]
        public string CourseCode { get; set; }

        [Display(Name = "Title")]
        [RequiredField(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        //[RegularExpression(@"([a-zA-Z ])", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [DataLength(MaximumLength: 50, MinimumLength: 2, ErrorMessage = busConstant.Messages.Type.Validations.Length)]
        public string Title { get; set; }

        public virtual ICollection<Entity.Enrollment> Enrollments { get; set; }
    }
}