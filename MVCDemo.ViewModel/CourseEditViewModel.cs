﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Helper;

namespace MVCDemo.ViewModel
{
    public class CourseEditViewModel : BaseViewModel
    {
        [Display(Name = "Course Code")]
        [Required(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = busConstant.Messages.Type.Validations.Length1to2)]
        public string CourseCode { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = busConstant.Messages.Type.Validations.Required)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = busConstant.Messages.Type.Validations.OnlyCharaters)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = busConstant.Messages.Type.Validations.Length2to50)]
        public string Title { get; set; }
    }
}
