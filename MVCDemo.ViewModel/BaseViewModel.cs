﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCDemo.ViewModel
{
    public abstract class BaseViewModel
    {
        [Display(Name = "Id")]
        public virtual long ID { get; set; }
       
        [Display(Name = "Created By")]
        public virtual string CreatedBy { get; set; }

        
        [Display(Name = "Created On")]
        [DataType(DataType.DateTime)]
        public virtual DateTime CreatedOn { get; set; }

       
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }

       
        [Display(Name = "Modified On")]
        [DataType(DataType.DateTime)]
        public virtual DateTime ModifiedOn { get; set; }

        
        [Display(Name = "Is Deleted")]
        public virtual bool IsDeleted { get; set; }

        [Display(Name = "Created On")]
        public string CreatedOns { get => CreatedOn.ToString("dd/MM/yyyy hh:mm:ss tt"); }

        [Display(Name = "Modified On")]
        public string ModifiedOns { get => ModifiedOn.ToString("dd/MM/yyyy hh:mm:ss tt"); }
    }
}