(function (angular) {
    'use strict';
    angular.module('ngAppStrictDemo', [])
        .controller('BadController', function ($scope) {
        $scope.a = 1;
        $scope.b = 2;
    })
        .controller('GoodController1', ['$scope', function ($scope) {
            $scope.a = 1;
            $scope.b = 2;
        }])
        .controller('GoodController2', GoodController2);
    function GoodController2($scope) {
        $scope.name = 'World';
    }
    GoodController2.$inject = ['$scope'];
})(window.angular);
