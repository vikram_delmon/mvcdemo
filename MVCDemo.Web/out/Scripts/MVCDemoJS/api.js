//We are using Restangular here, the code bellow will just make an ajax request to /api/states & /api/customers?{filtercriteria}  
angular.module('mvcDemo').factory('api', function (Restangular) {
    alert(2);
    //prepend /api before making any request with restangular
    RestangularProvider.setBaseUrl('/api');
    return {
        states: function () {
            return Restangular.all("states").getList();
        },
        customers: {
            search: function (query) {
                return Restangular.all('customers').getList(query);
            }
        },
        course: {
            search: function (query) {
                return Restangular.all('course').getList(query);
            }
        }
    };
});
