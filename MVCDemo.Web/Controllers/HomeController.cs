﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCDemo.DataAccess;
using MVCDemo.DataAccess.Implementations;
using MVCDemo.Web.Utils;

namespace MVCDemo.Web.Controllers
{
    public class HomeController : BaseController
    {
        #region [Fields]

        private UnitOfWork unitOfWork;
        #endregion

        #region [Constructors]

        public HomeController()
        {
            this.unitOfWork = new UnitOfWork();

        }
        public HomeController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        #endregion

        #region [Methos]

        [AllowAnonymous]
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Popup1()
        {
            return View();
        }

        #endregion
    }
}