﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MVCDemo.Helper;
using Omu.AwesomeMvc;
using MVCDemo.Web.Filters;
using System.Transactions;
using AutoMapper;
using AutoMapper.EntityFramework;
using AutoMapper.Collection;
using AutoMapper.Collection.LinqToSQL;
using System.Net;
using MVCDemo.Web.Models;
using MVCDemo.ViewModel;

namespace MVCDemo.Web.Controllers
{
    public class CoursesController : BaseController
    {
        #region [Field & Contructor]


        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CoursesController()
        {
        }

        public CoursesController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        #endregion
        [HttpPost]
        [AllowAnonymous]
        [AuditAttribute]
        [ValidateAntiForgeryTokens]
        [Route("Post/Courses/CreateAsync")]
        public async Task<ActionResult> CreateAsync(CourseCreateViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ValidationResponse();
                }
                else
                {
                    using (var transaction = new TransactionScope())
                    {
                        try
                        {                            
                            var entity = new Entity.Course();
                            Mapper.Map(model, entity);
                            unitOfWork.CourseRepository.Insert(entity);
                            transaction.Complete();
                            var response = new JsonResponse()
                            {
                                Type = busConstant.Messages.Type.Response,
                                Message = busConstant.Messages.Type.Responses.Save,
                                Errors = null,
                                ReturnUrl = null
                            };
                            return Json(response);
                        }
                        catch
                        {
                            transaction.Dispose();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ExceptionResponse(ex);
            }
            finally
            {
                unitOfWork.Dispose();
            }
        }

        // GET: Courses
        [HttpGet]
        [AllowAnonymous]
        //[Route(busConstant.Forms.LookUp)]
        public async Task<ActionResult> Create()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Get/Courses/EditAsync")]
        public async Task<ActionResult> EditAsync(long Id)
        {
            try
            {
                if (Id == default(long))
                {
                    var response = new JsonResponse()
                    {
                        Type = busConstant.Messages.Type.Response,
                        Message = busConstant.Messages.Type.Responses.Save,
                        Errors = null
                    };
                    Response.StatusCode = Convert.ToInt32(HttpStatusCode.BadRequest);
                    return Json(response);
                }
                else
                {
                    var entity = unitOfWork.CourseRepository.GetByID(Id);
                    if (entity.IsNotNull())
                    {
                        var response = new JsonResponse()
                        {
                            Type = busConstant.Messages.Type.Response,
                            Message = null,
                            Errors = null,
                            Entity = entity
                        };
                        return Json(response);
                    }
                    else
                    {
                        var response = new JsonResponse()
                        {
                            Type = busConstant.Messages.Type.Response,
                            Message = null,
                            Errors = null,
                            Entity = null
                        };
                        return Json(response);
                    }
                }
            }
            catch (Exception ex)
            {
                return ExceptionResponse(ex);
            }
            finally
            {
                unitOfWork.Dispose();
            }
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult IndexAsync()
        {
            //Datatable parameter
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            //paging parameter
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //sorting parameter            
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            //Global filter parameter
            var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
            //Column filter parameter
            var searchValueID = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var searchValueCourseCode = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var searchValueTitle = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var searchValueCreatedBy = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var searchValueCreatedOn = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var searchValueModifiedBy = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
            var searchValueModifiedOn = Request.Form.GetValues("columns[6][search][value]").FirstOrDefault();
            //For Mapping domain entity to view model
            IEnumerable<CourseIndexViewModel> list = new List<CourseIndexViewModel>();
            IEnumerable<Entity.Course> courses; // = new List<Entity.Course>();

            int pageSize = length != null ? Convert.ToInt32(length) : busConstant.Numbers.Integer.Zero;
            int skip = start != null ? Convert.ToInt32(start) : busConstant.Numbers.Integer.Zero;
            int totalRecords = busConstant.Numbers.Integer.Zero;

            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                totalRecords = unitOfWork.CourseRepository.Count(x => x.ID.ToString().Contains(searchValue) || x.CourseCode.Contains(searchValue) || x.Title.Contains(searchValue) || x.CreatedBy.Contains(searchValue) || x.ModifiedBy.Contains(searchValue) || x.CreatedOn.ToString().Contains(searchValue) || x.ModifiedOn.ToString().Contains(searchValue));
                //Database query
                pageSize = pageSize < busConstant.Numbers.Integer.Zero ? totalRecords : pageSize;
                //list = unitOfWork.CourseRepository.Get(x => x.ID.ToString().Contains(searchValue) || x.CourseCode.Contains(searchValue) || x.Title.Contains(searchValue) || x.CreatedBy.Contains(searchValue) || x.ModifiedBy.Contains(searchValue) || x.CreatedOn.ToString().Contains(searchValue) || x.ModifiedOn.ToString().Contains(searchValue)).OrderBy(sortColumn + " " + sortColumnDir).Skip(skip).Take(pageSize).ToList().Select(e => new CourseIndexViewModel
                //{
                //    ID = e.ID,
                //    CourseCode = e.CourseCode,
                //    Title = e.Title,
                //    CreatedBy = e.CreatedBy,
                //    CreatedOn = e.CreatedOn,
                //    ModifiedBy = e.ModifiedBy,
                //    ModifiedOn = e.ModifiedOn,
                //    IsDeleted = e.IsDeleted
                //});

                courses = unitOfWork.CourseRepository.Get(x => x.ID.ToString().Contains(searchValue) || x.CourseCode.Contains(searchValue) || x.Title.Contains(searchValue) || x.CreatedBy.Contains(searchValue) || x.ModifiedBy.Contains(searchValue) || x.CreatedOn.ToString().Contains(searchValue) || x.ModifiedOn.ToString().Contains(searchValue)).OrderBy(sortColumn + " " + sortColumnDir).Skip(skip).Take(pageSize). ToList();

            }

            AutoMapper.Mapper.Map(courses, list);
            var response = new { draw = Draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = list };
            return Json(data: response);
        }

        //[HttpGet]
        //public async Task<ActionResult> IndexAweAsync(GridParams grdParams, CourseIndexViewModel filter)
        //{
        //    var pageSize = grdParams.GetPageSize();
        //    var page = grdParams.GetPage();
        //    var sortExpression = grdParams.GetSortExpression("FarmerId");
        //    //filter.HomePhoneNo = filter.CellPhoneNo.IsNotNullOrEmpty() ? filter.CellPhoneNo : null;
        //    var list = await unitOfWork.CourseRepository.GetAsync(x => x.ID.Equals(filter.ID) || x.CourseCode.Equals(filter.CourseCode) || x.Title.Equals(filter.Title) || x.CreatedBy.Equals(filter.CreatedBy) || x.ModifiedBy.Equals(filter.ModifiedBy) || x.CreatedOn.Equals(filter.CreatedOn) || x.ModifiedOn.Equals(filter.ModifiedOn)).OrderBy(sortColumn + " " + sortColumnDir).Skip(skip).Take(pageSize).ToList();
        //    //var list = await Farmer.GetPageAsync(page, pageSize, sortExpression, filter);

        //    var count = list.Count;
        //    var totalPages = (int)Math.Ceiling((float)count / pageSize);

        //    return Json(new GridModelBuilder<VW_FarmerIndexIndexViewModel>(list.ItemList.AsQueryable(), grdParams)
        //    {
        //        KeyProp = x => x.FarmerId,
        //        Map = x => new
        //        {
        //            x.FarmerId,
        //            x.LastName,
        //            x.FirstName,
        //            x.MiddleName,
        //            x.GenderValue,
        //            x.CellPhoneNo,
        //            x.HomePhoneNo,
        //            x.WorkPhoneNo,
        //            x.AddressId,
        //            x.AddressLine1,
        //            x.ZipCode,
        //            x.CountryValue,
        //            x.StateValue,
        //            x.CityValue,
        //            x.TalukaValue,
        //            x.VillageValue
        //        },
        //        PageCount = totalPages,
        //        ItemsCount = count
        //    }.Build(), JsonRequestBehavior.AllowGet);

        //}

    }
}