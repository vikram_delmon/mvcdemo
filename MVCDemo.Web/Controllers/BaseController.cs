﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MVCDemo.Helper;
using Omu.AwesomeMvc;
using MVCDemo.DataAccess.Implementations;

namespace MVCDemo.Web.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        protected UnitOfWork unitOfWork;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public BaseController()
        {
            this.unitOfWork = new UnitOfWork();

        }
        public BaseController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public BaseController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }


        // GET: Base

        protected ActionResult ValidationResponse(JsonRequestBehavior jsonRequestBehaviour = JsonRequestBehavior.AllowGet)
        {
            if (ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            var validationMessages = new List<JsonValidationError>();
            var validationSummary = new List<string>();
            foreach (var key in ModelState.Keys)
            {
                ModelState modelState = null;
                if (ModelState.TryGetValue(key, out modelState))
                {
                    foreach (var error in modelState.Errors)
                    {
                        validationMessages.Add(new JsonValidationError()
                        {
                            Key = key,
                            Message = error.ErrorMessage
                        });
                    }
                }

            }

            validationSummary = ModelState.Keys.SelectMany(k => ModelState[k].Errors).DistinctBy(k=>k.ErrorMessage)
                            .Select(m => m.ErrorMessage).ToList();


            var response = new JsonResponse()
            {
                Type = busConstant.Messages.Type.Validation,
                Message = "",
                Errors = validationMessages,
                ValidationSummary = validationSummary
            };

            Response.StatusCode = Convert.ToInt32(HttpStatusCode.BadRequest);
            return Json(response, jsonRequestBehaviour);
        }

        protected ActionResult ExceptionResponse(Exception ex, JsonRequestBehavior jsonRequestBehaviour = JsonRequestBehavior.AllowGet)
        {
            var response = new JsonResponse()
            {
                Type = busConstant.Messages.Type.Exception,
                Message = busConstant.Messages.Type.Exceptions.InternalServerError,
                Errors = null
            };

            Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
            return Json(response, jsonRequestBehaviour);
        }
    }
}