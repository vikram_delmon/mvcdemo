using System;
using System.Web.Mvc;
using MVCDemo.ViewModel;

namespace MVCDemo.Web.Models.Error
{
    public class CustomHandleErrorInfo : HandleErrorInfo
    {
        public string DisplayErrorMessage { get; set; }

        public CustomHandleErrorInfo(Exception exception, string controllerName, string actionName)
            : base(exception, controllerName, actionName)
        {
        }
    }
}