﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MVCDemo.Helper;
using MVCDemo.ViewModel;

namespace MVCDemo.Web.Filters
{
    public class AuditAttribute : ActionFilterAttribute, IActionFilter
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var _activityDateTime = System.DateTime.Now;
            var _activityBy = filterContext.HttpContext.User.Identity.IsAuthenticated ? filterContext.HttpContext.User.Identity.GetUserName() : busConstant.Misc.SYSTEM;

            var model = filterContext.ActionParameters["model"] as BaseViewModel;
            if (model.IsNotNull())
            {
                if (model.ID == default(long))
                {
                    model.CreatedBy = _activityBy;
                    model.CreatedOn = _activityDateTime;
                    model.ModifiedBy = _activityBy;
                    model.ModifiedOn = _activityDateTime;
                }
                else
                {
                    model.ModifiedBy = _activityBy;
                    model.ModifiedOn = _activityDateTime;
                }
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
    }

}
