﻿var app = angular.module('mvcDemo', ['datatables']);
app.controller('formCourseCtrl', function ($scope,$location, $http) {

    $scope.SaveButton = 'btn btn-success';
    $scope.ResetButton = 'btn btn-default';

    $scope.ResetForm = function () {
        $scope.SaveButton = 'btn btn-success';
        document.getElementById('cont').removeAttribute('hidden');
        $scope.errors = {};
        $scope.errors.pageError = {};
        $scope.errors.formErrors = {};
        $scope.errors.pageError = null;
        $scope.errors.formErrors = null;
        $scope.CourseCreateViewModel = {};
        $scope.CourseCode = null;
        $scope.Title = null;
        $scope.ValidationSummary = null;       
    };

    $scope.SaveForm = function () {
        document.getElementById('cont').setAttributeNode(document.createAttribute('hidden'));
        $scope.SaveButton = 'btn btn-success disabled';
        $scope.CourseCreateViewModel = {};
        $scope.CourseCreateViewModel.CourseCode = $scope.CourseCode;
        $scope.CourseCreateViewModel.Title = $scope.Title;

        $http.post("/Post/Courses/CreateAsync", $scope.CourseCreateViewModel,
            {
                headers: { 'RequestVerificationToken': $scope.antiForgeryToken }
            }
        ).then(
            function success(response) {                
                // Add your success stuff here
                swal(
                    'Success!',
                    response.data.Message,
                    'success'
                );
                $scope.ResetForm();
                //$scope.SaveButton = 'btn btn-success';               
                //document.getElementById('cont').removeAttribute('hidden');
                //console.log(response);               
                //window.location.href=response.ReturnUrl;

            }, function errors(response) {
                //https://benjii.me/2014/10/handling-validation-errors-with-angularjs-and-asp-net-mvc/
                handleErrors(response.data);
                console.clear();
                $scope.SaveButton = 'btn btn-success';
                document.getElementById('cont').removeAttribute('hidden');
            });

        function updateErrors(errors) {
            $scope.errors = {};
            $scope.errors.formErrors = {};
            $scope.errors.pageError = "";
            if (errors) {
                for (var i = 0; i < errors.length; i++) {
                    $scope.errors.formErrors[errors[i].Key] = errors[i].Message;
                }
            }
        }

        var handleErrors = function (data) {
            if (data.ValidationSummary) {
                $scope.ValidationSummary = {};
                $scope.ValidationSummary = data.ValidationSummary;
            }
            if (data.Errors) {
                updateErrors(data.Errors);
            } else if (data.Message) {
                $scope.errors = {};
                $scope.errors.pageError = data.Message;
            } else if (data) {
                $scope.errors = {};
                $scope.errors.pageError = data;
            } else {
                $scope.errors = {};
                $scope.errors.pageError = "An unexpected error has occurred, please try again later.";
            }
        };

    }

    $scope.LoadForm = function () {

        if ($location.search().hasOwnProperty('myparam')) {
            var myvalue = $location.search()['myparam'];
            // 'myvalue' now stores '33'
        }

        $http.post("/get/Courses/EditAsync", $scope.CourseCreateViewModel,
            {
                headers: { 'RequestVerificationToken': $scope.antiForgeryToken }
            }
        ).then(
            function success(response) {
                // Add your success stuff here
                swal(
                    'Success!',
                    response.data.Message,
                    'success'
                );
                $scope.ResetForm();
                //$scope.SaveButton = 'btn btn-success';               
                //document.getElementById('cont').removeAttribute('hidden');
                //window.location = response.locations;

            }, function errors(response) {
                //https://benjii.me/2014/10/handling-validation-errors-with-angularjs-and-asp-net-mvc/
                handleErrors(response.data);
                console.clear();
                $scope.SaveButton = 'btn btn-success';
                document.getElementById('cont').removeAttribute('hidden');
            });
    };

});

//app.controller('gridCourseCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnBuilder',
//    function ($scope, $http, DTOptionsBuilder, DTColumnBuilder) {
//        $scope.dtColumns = [
//            //here We will add .withOption('name','column_name') for send column name to the server 
//            DTColumnBuilder.newColumn("ID", "Course ID").withOption('name', 'ID'),
//            DTColumnBuilder.newColumn("CourseCode", "Course Code").withOption('name', 'CourseCode'),
//            DTColumnBuilder.newColumn("Title", "Title").withOption('name', 'Title'),
//            DTColumnBuilder.newColumn("CreatedBy", "Created By").withOption('name', 'CreatedBy'),
//            DTColumnBuilder.newColumn("CreatedOn", "Created On").withOption('name', 'CreatedOn').renderWith(function (data, type) {
//                // return formatJSONDate(data);  
//                return $filter('CreatedOn')(data, 'dd/MM/yyyy'); //date filter  

//            }),
//            DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
//            DTColumnBuilder.newColumn("ModifiedOn", "Modified On").withOption('name', 'ModifiedOn')

//        ]
//        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
//            dataSrc: "data",
//            url: "/Courses/IndexAsync",
//            type: "POST"
//        })
//            .withOption('processing', true) //for show progress bar
//            .withOption('serverSide', true) // for server side processing
//            .withPaginationType('full_numbers') // for get full pagination options // first / last / prev / next and page numbers
//            .withDisplayLength(10) // Page size
//            .withOption('aaSorting', [0, 'asc']) // for default sorting column // here 0 means first column
//    }])

app.controller('formStudentCtrl', function ($scope, $http) {
    $scope.SaveButton = 'btn btn-success';
    $scope.ResetButton = 'btn btn-default';

    $scope.ResetForm = function () {
        $scope.SaveButton = 'btn btn-success';
        document.getElementById('cont').removeAttribute('hidden');
        $scope.errors.pageError = {};
        $scope.errors.formErrors = {};
        $scope.errors.pageError = null;
        $scope.errors.formErrors = null;
        $scope.CourseCreateViewModel = {};
        $scope.CourseCode = null;
        $scope.Title = null;
        $scope.ValidationSummary = null;
    };

    $scope.SaveForm = function () {
        document.getElementById('cont').setAttributeNode(document.createAttribute('hidden'));
        $scope.SaveButton = 'btn btn-success disabled';
        $scope.StudentCreateViewModel = {};
        $scope.StudentCreateViewModel.CourseCode = $scope.CourseCode;
        $scope.StudentCreateViewModel.Title = $scope.Title;

        $http.post("/Student/Create", $scope.CourseCreateViewModel,
            {
                headers: { 'RequestVerificationToken': $scope.antiForgeryToken }
            }
        ).then(
            function success(response) {
                // Add your success stuff here
                swal(
                    'Success!',
                    response.data.Message,
                    'success'
                );
                $scope.ResetForm();
                //$scope.SaveButton = 'btn btn-success';               
                //document.getElementById('cont').removeAttribute('hidden');
                //window.location = response.locations;

            }, function errors(response) {
                //https://benjii.me/2014/10/handling-validation-errors-with-angularjs-and-asp-net-mvc/
                handleErrors(response.data);
                console.clear();
                $scope.SaveButton = 'btn btn-success';
                document.getElementById('cont').removeAttribute('hidden');
            });

        function updateErrors(errors) {
            $scope.errors = {};
            $scope.errors.formErrors = {};
            $scope.errors.pageError = "";
            if (errors) {
                for (var i = 0; i < errors.length; i++) {
                    $scope.errors.formErrors[errors[i].Key] = errors[i].Message;
                }
            }
        }

        var handleErrors = function (data) {
            if (data.ValidationSummary) {
                $scope.ValidationSummary = {};
                $scope.ValidationSummary = data.ValidationSummary;
            }
            if (data.Errors) {
                updateErrors(data.Errors);
            } else if (data.message) {
                $scope.errors = {};
                $scope.errors.pageError = data.message;
            } else if (data) {
                $scope.errors = {};
                $scope.errors.pageError = data;
            } else {
                $scope.errors = {};
                $scope.errors.pageError = "An unexpected error has occurred, please try again later.";
            }
        };

    }

});
