﻿using MVCDemo.Helper;
using System.Web;
using System.Web.Optimization;

namespace MVCDemo.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/MVCDemo/Styles").Include(
                    "~/MasterPage/bower_components/bootstrap/dist/css/bootstrap.css",
                    "~/MasterPage/bower_components/font-awesome/css/font-awesome.css",
                    "~/MasterPage/bower_components/Ionicons/css/ionicons.css",
                    "~/MasterPage/bower_components/datatables.net-bs/css/dataTables.bootstrap.css",
                    //"~/MasterPage/angularjsDataTables/css/jquery.dataTables.min.css",
                    "~/MasterPage/bower_components/bootstrap-daterangepicker/daterangepicker.css",
                    "~/MasterPage/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css",
                    "~/MasterPage/plugins/iCheck/all.css",
                    "~/MasterPage/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css",
                    "~/MasterPage/plugins/timepicker/bootstrap-timepicker.css",
                    "~/MasterPage/bower_components/select2/dist/css/select2.css",
                    "~/MasterPage/bower_components/jvectormap/jquery-jvectormap.css",
                    "~/MasterPage/bower_components/morris.js/morris.css",
                    "~/MasterPage/dist/css/AdminLTE.css",
                    "~/MasterPage/dist/css/skins/_all-skins.css",
                    "~/MasterPage/plugins/iCheck/flat/blue.css",
                    "~/MasterPage/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css",
                    "~/Content/sweetalert2.css"
                ));

            //bundles.Add(new StyleBundle("~/bundles/MVCDemo/Styles").Include(GetStyles()));

            bundles.Add(new ScriptBundle("~/bundles/MVCDemo/Scripts").Include(
                   // "~/MasterPage/angularjsDataTables/scripts/jquery.js",
                    "~/MasterPage/bower_components/jquery/dist/jquery.js",
                    "~/MasterPage/bower_components/bootstrap/dist/js/bootstrap.js",
                    "~/MasterPage/bower_components/datatables.net/js/jquery.dataTables.js",
                    //"~/MasterPage/angularjsDataTables/scripts/jquery.dataTables.js",
                    "~/MasterPage/bower_components/datatables.net-bs/js/dataTables.bootstrap.js",
                    "~/MasterPage/bower_components/select2/dist/js/select2.full.js",
                    "~/MasterPage/plugins/input-mask/jquery.inputmask.js",
                    "~/MasterPage/plugins/input-mask/jquery.inputmask.date.extensions.js",
                    "~/MasterPage/plugins/input-mask/jquery.inputmask.extensions.js",
                    "~/MasterPage/bower_components/moment/min/moment.min.js",
                    "~/MasterPage/bower_components/bootstrap-daterangepicker/daterangepicker.js",
                    "~/MasterPage/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js",
                    "~/MasterPage/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js",
                    "~/MasterPage/plugins/timepicker/bootstrap-timepicker.js",
                    "~/MasterPage/bower_components/jquery-slimscroll/jquery.slimscroll.js",
                    "~/MasterPage/plugins/iCheck/icheck.js",
                    "~/MasterPage/bower_components/fastclick/lib/fastclick.js",
                    "~/MasterPage/dist/js/adminlte.js",
                    "~/MasterPage/bower_components/jquery-sparkline/dist/jquery.sparkline.js",
                    "~/MasterPage/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                    "~/MasterPage/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                    "~/MasterPage/bower_components/jquery-slimscroll/jquery.slimscroll.js",
                    "~/MasterPage/bower_components/chart.js/Chart.js",
                    "~/MasterPage/bower_components/raphael/raphael.js",
                    "~/MasterPage/bower_components/morris.js/morris.js",
                    //"~/MasterPage/dist/js/pages/dashboard2.js",// AdminLTE dashboard demo (This is only for demo purposes)
                    //"~/MasterPage/dist/js/demo.js",//AdminLTE for demo purposes
                    "~/MasterPage/plugins/iCheck/icheck.js",
                    "~/MasterPage/bower_components/Flot/jquery.flot.js",
                    "~/MasterPage/bower_components/Flot/jquery.flot.resize.js",
                    "~/MasterPage/bower_components/Flot/jquery.flot.pie.js",
                    "~/MasterPage/bower_components/Flot/jquery.flot.categories.js",
                    "~/MasterPage/bower_components/jquery-knob/js/jquery.knob.js",
                    "~/MasterPage/bower_components/ckeditor/ckeditor.js",
                    "~/MasterPage/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js",
                    "~/Scripts/sweetalert2.all.js",
                    "~/Scripts/angular-1.6.9/angular.js",
                    "~/MasterPage/angularjsDataTables/scripts/angular-datatables.js",
                    "~/Scripts/MVCDemoJS/MVCDemo.js"
                ));

            //BundleTable.EnableOptimizations = busConstant.Misc.TRUE;
            //bundles.Add(new ScriptBundle("~/bundles/MVCDemo/Scripts").Include(GetScripts()));
        }

        private static string[] GetStyles()
        {
            return new[]
            {
                    "~/MasterPage/bower_components/bootstrap/dist/css/bootstrap.css",
                    "~/MasterPage/bower_components/font-awesome/css/font-awesome.css",
                    "~/MasterPage/bower_components/Ionicons/css/ionicons.css",
                    "~/MasterPage/bower_components/datatables.net-bs/css/dataTables.bootstrap.css",
                    "~/MasterPage/bower_components/bootstrap-daterangepicker/daterangepicker.css",
                    "~/MasterPage/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css",
                    "~/MasterPage/plugins/iCheck/all.css",
                    "~/MasterPage/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css",
                    "~/MasterPage/plugins/timepicker/bootstrap-timepicker.css",
                    "~/MasterPage/bower_components/select2/dist/css/select2.css",
                    "~/MasterPage/bower_components/jvectormap/jquery-jvectormap.css",
                    "~/MasterPage/bower_components/morris.js/morris.css",
                    "~/MasterPage/dist/css/AdminLTE.css",
                    "~/MasterPage/dist/css/skins/_all-skins.css",
                    "~/MasterPage/plugins/iCheck/flat/blue.css",
                    "~/MasterPage/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css",
                    "~/Scripts/angular-1.6.9/angular.js"
            };


        }

        private static string[] GetScripts()
        {
            return new[]
            {
               "~/MasterPage/bower_components/jquery/dist/jquery.js",
               "~/MasterPage/bower_components/bootstrap/dist/js/bootstrap.js",
               "~/MasterPage/bower_components/datatables.net/js/jquery.dataTables.js",
               "~/MasterPage/bower_components/datatables.net-bs/js/dataTables.bootstrap.js",
               "~/MasterPage/bower_components/select2/dist/js/select2.full.js",
               "~/MasterPage/plugins/input-mask/jquery.inputmask.js",
               "~/MasterPage/plugins/input-mask/jquery.inputmask.date.extensions.js",
               "~/MasterPage/plugins/input-mask/jquery.inputmask.extensions.js",
               "~/MasterPage/bower_components/moment/min/moment.min.js",
               "~/MasterPage/bower_components/bootstrap-daterangepicker/daterangepicker.js",
               "~/MasterPage/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js",
               "~/MasterPage/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js",
               "~/MasterPage/plugins/timepicker/bootstrap-timepicker.js",
               "~/MasterPage/bower_components/jquery-slimscroll/jquery.slimscroll.js",
               "~/MasterPage/plugins/iCheck/icheck.js",
               "~/MasterPage/bower_components/fastclick/lib/fastclick.js",
               "~/MasterPage/dist/js/adminlte.js",
               "~/MasterPage/bower_components/jquery-sparkline/dist/jquery.sparkline.js",
               "~/MasterPage/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
               "~/MasterPage/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
               "~/MasterPage/bower_components/jquery-slimscroll/jquery.slimscroll.js",
               "~/MasterPage/bower_components/chart.js/Chart.js",
               "~/MasterPage/bower_components/raphael/raphael.js",
               "~/MasterPage/bower_components/morris.js/morris.js",
               "~/MasterPage/dist/js/pages/dashboard2.js",// AdminLTE dashboard demo (This is only for demo purposes)
               "~/MasterPage/dist/js/demo.js",//AdminLTE for demo purposes
               "~/MasterPage/plugins/iCheck/icheck.js",
               "~/MasterPage/bower_components/Flot/jquery.flot.js",
               "~/MasterPage/bower_components/Flot/jquery.flot.resize.js",
               "~/MasterPage/bower_components/Flot/jquery.flot.pie.js",
               "~/MasterPage/bower_components/Flot/jquery.flot.categories.js",
               "~/MasterPage/bower_components/jquery-knob/js/jquery.knob.js",
               "~/MasterPage/bower_components/ckeditor/ckeditor.js",
               "~/MasterPage/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"
            };
        }
    }
}
