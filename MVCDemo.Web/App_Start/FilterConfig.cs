﻿using System.Web;
using System.Web.Mvc;
using MVCDemo.Web.Filters;

namespace MVCDemo.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new ExceptionFilters());
        }
    }
}
