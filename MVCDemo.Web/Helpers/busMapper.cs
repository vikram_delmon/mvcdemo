﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Entity;
using MVCDemo.ViewModel;
using AutoMapper;
using AutoMapper.Collection;
using AutoMapper.EntityFramework;
using AutoMapper.Collection.LinqToSQL;

namespace MVCDemo.Web
{
    public class busMapper : AutoMapper.Profile
    {
        public busMapper()
        {
            CreateMap<Entity.Course, ViewModel.CourseIndexViewModel>();
            CreateMap<Entity.Course, ViewModel.CourseEditViewModel>();
            CreateMap<ViewModel.CourseEditViewModel, Entity.Course>();
            CreateMap<ViewModel.CourseCreateViewModel, Entity.Course>();
        }

        public static void Run()
        {
            AutoMapper.Mapper.Initialize(map =>
            {
                map.AddProfile<busMapper>();

            });
        }
    }
}
