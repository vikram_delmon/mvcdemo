﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.DataAccess.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        //void Delete<TEntity>(TEntity entity) where TEntity : class;
        //IQueryable<TEntity> GetAll<TEntity>() where TEntity : class;
        //void Insert<TEntity>(TEntity entity) where TEntity : class;
        //void Update<TEntity>(TEntity entity) where TEntity : class;
        //void SaveChanges();



        IEnumerable<TEntity> Get();
        TEntity GetByID(int ID);
        void Insert(TEntity entity);
        void Delete(int ID);
        void Update(TEntity entity);
        void Save();
        int GetCount();
        IEnumerable<TEntity> SelectAll(int maximumRows, int startRowIndex);
        IEnumerable<TEntity> SelectAll(Expression<Func<TEntity, object>> sortExpression);
        IEnumerable<TEntity> SelectAll(Expression<Func<TEntity, object>> sortExpression, int maximumRows, int startRowIndex);
    }
}
