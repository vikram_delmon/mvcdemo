﻿using MVCDemo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.DataAccess.Interfaces
{
    public interface ICourseRepository: IDisposable
    {
        IEnumerable<Course> Get();
        Course GetByID(int ID);
        void Insert(Course entity);
        void Delete(int ID);
        void Update(Course entity);
        void Save();
    }
}
