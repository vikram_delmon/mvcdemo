﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Entity;

namespace MVCDemo.DataAccess.Interfaces
{
    public interface IStudentRepository : IDisposable
    {
        IEnumerable<Student> Get();
        Student GetByID(int ID);
        void Insert(Student student);
        void Delete(int ID);
        void Update(Student student);
        void Save();
    }
}
