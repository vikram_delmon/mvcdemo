namespace MVCDemo.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Created_By = c.String(),
                        Created_On = c.DateTime(nullable: false),
                        Modified_By = c.String(),
                        Modified_On = c.DateTime(nullable: false),
                        Is_Active = c.Boolean(nullable: false),
                        Is_Deleted = c.Boolean(nullable: false),
                        Update_Seq = c.Int(nullable: false),
                        Customer_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customer", t => t.Customer_ID)
                .Index(t => t.Customer_ID);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Contact_Person = c.String(),
                        Contact_Number = c.String(),
                        Email = c.String(),
                        GST = c.String(),
                        No_Of_Locations = c.Int(nullable: false),
                        No_Of_Inventory_Items = c.Int(nullable: false),
                        Created_By = c.String(),
                        Created_On = c.DateTime(nullable: false),
                        Modified_By = c.String(),
                        Modified_On = c.DateTime(nullable: false),
                        Is_Active = c.Boolean(nullable: false),
                        Is_Deleted = c.Boolean(nullable: false),
                        Update_Seq = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Code",
                c => new
                    {
                        ID = c.Long(nullable: false),
                        Data_1_Caption = c.String(maxLength: 100),
                        Description = c.String(maxLength: 100),
                        Data_1_Type = c.String(maxLength: 4),
                        Data_2_Caption = c.String(maxLength: 100),
                        Data_2_Type = c.String(maxLength: 4),
                        Data_3_Caption = c.String(maxLength: 100),
                        Data_3_Type = c.String(maxLength: 4),
                        Comments = c.String(),
                        Created_By = c.String(),
                        Created_On = c.DateTime(nullable: false),
                        Modified_By = c.String(),
                        Modified_On = c.DateTime(nullable: false),
                        Is_Active = c.Boolean(nullable: false),
                        Is_Deleted = c.Boolean(nullable: false),
                        Update_Seq = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ExceptionLog",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Message = c.String(),
                        Inner_Exception = c.String(),
                        Source = c.String(),
                        Stack_Trace = c.String(),
                        Target_Site = c.String(),
                        HResult = c.Int(nullable: false),
                        Help_Link = c.String(),
                        Controller_Name = c.String(),
                        Action_Name = c.String(),
                        Url = c.String(),
                        Created_By = c.String(),
                        Created_On = c.DateTime(nullable: false),
                        Modified_By = c.String(),
                        Modified_On = c.DateTime(nullable: false),
                        Is_Active = c.Boolean(nullable: false),
                        Is_Deleted = c.Boolean(nullable: false),
                        Update_Seq = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Category", "Customer_ID", "dbo.Customer");
            DropIndex("dbo.Category", new[] { "Customer_ID" });
            DropTable("dbo.ExceptionLog");
            DropTable("dbo.Code");
            DropTable("dbo.Customer");
            DropTable("dbo.Category");
        }
    }
}
