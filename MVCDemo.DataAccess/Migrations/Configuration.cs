namespace MVCDemo.DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using MVCDemo.Helper;

    internal sealed class Configuration : DbMigrationsConfiguration<MVCDemo.DataAccess.MVCDemoDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = busConstant.Misc.TRUE;
            ContextKey = "MVCDemo.DataAccess.MVCDemoDBContext";
        }

        protected override void Seed(MVCDemo.DataAccess.MVCDemoDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
