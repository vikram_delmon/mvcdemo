﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.DataAccess.Interfaces;
using MVCDemo.Entity;

namespace MVCDemo.DataAccess.Implementations
{
    public class StudentRepository : IStudentRepository, IDisposable
    {
        private MVCDemoDBContext context;

        public StudentRepository(MVCDemoDBContext context)
        {
            this.context = context;
        }

        public void Delete(int ID)
        {
            Student entity = context.Students.Find(ID);
            //context.Students.Remove(student);
            entity.IsDeleted = true;
            entity.update("user");
            context.Entry(entity).State = EntityState.Modified;
        }

        public IEnumerable<Student> Get()
        {
            return context.Students.ToList();
        }

        public Student GetByID(int ID)
        {
            return context.Students.Find(ID);
        }

        public void Insert(Student entity)
        {
            entity.create("user");
            this.context.Students.Add(entity);
        }
        public void Update(Student entity)
        {
            entity.update("user");
            context.Entry(entity).State = EntityState.Modified;
        }
        public void Save()
        {
            this.context.SaveChanges();
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    this.context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }       

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
