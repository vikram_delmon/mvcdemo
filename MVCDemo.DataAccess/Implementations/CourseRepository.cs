﻿using MVCDemo.DataAccess.Interfaces;
using MVCDemo.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCDemo.DataAccess.Implementations
{
   public class CourseRepository : ICourseRepository
    {
        private MVCDemoDBContext context;

        public CourseRepository(MVCDemoDBContext context)
        {
            this.context = context;
        }

        public void Delete(int ID)
        {
            Course entity = this.context.Courses.Find(ID);
            //this.context.Courses.Remove(entity);
            entity.IsDeleted = true;
            entity.update("user");
            this.context.Entry(entity).State = EntityState.Modified;
        }

        public IEnumerable<Course> Get()
        {
            return this.context.Courses.ToList();
        }

        public Course GetByID(int ID)
        {
            return this.context.Courses.Find(ID);
        }

        public void Insert(Course entity)
        {
            entity.create("user");
            this.context.Courses.Add(entity);
        }
        public void Update(Student entity)
        {
            entity.update("user");
            this.context.Entry(entity).State = EntityState.Modified;
        }
        public void Save()
        {
            this.context.SaveChanges();
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    this.context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

       

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
               
        #endregion
    }
}
