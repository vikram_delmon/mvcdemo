﻿using System;
using System.Transactions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCDemo.Entity;

namespace MVCDemo.DataAccess.Implementations
{
    public class UnitOfWork : IDisposable
    {
        #region [Fields]

        //private readonly long _LoginID;
        #endregion

        #region [Proerties]

        private bool disposed = false;
        private TransactionScope transaction;

        private MVCDemoDBContext context = new MVCDemoDBContext();

        private GenericRepository<Code> _CodeRepository;
        private GenericRepository<CodeValue> _CodeValueRepository;
        private GenericRepository<Customer> _CustomerRepository;
        private GenericRepository<Category> _CategoryRepository;
        //private GenericRepository<Customer> studentRepository;
        //private GenericRepository<Course> courseRepository;
        //private GenericRepository<Enrollment> enrollmentRepository;
        //private GenericRepository<Grade> gradeRepository;
        private GenericRepository<ExceptionLog> exceptionLogRepository;


        public GenericRepository<Code> CodeRepository
        {
            get
            {
                if (this._CodeRepository == null)
                {
                    this._CodeRepository = new GenericRepository<Code>(context);
                }
                return _CodeRepository;
            }
        }

        public GenericRepository<CodeValue> CodeValueRepository
        {
            get
            {
                if (this._CodeValueRepository == null)
                {
                    this._CodeValueRepository = new GenericRepository<CodeValue>(context);
                }
                return _CodeValueRepository;
            }
        }

        public GenericRepository<Customer> CustomerRepository
        {
            get
            {
                if (this._CustomerRepository == null)
                {
                    this._CustomerRepository = new GenericRepository<Customer>(context);
                }
                return _CustomerRepository;
            }
        }

        public GenericRepository<Category> CategoryRepository
        {
            get
            {
                if (this._CategoryRepository == null)
                {
                    this._CategoryRepository = new GenericRepository<Category>(context);
                }
                return _CategoryRepository;
            }
        }

        //public GenericRepository<Course> CourseRepository
        //{
        //    get
        //    {

        //        if (this.courseRepository == null)
        //        {
        //            this.courseRepository = new GenericRepository<Course>(context);
        //        }
        //        return courseRepository;
        //    }
        //}

        //public GenericRepository<Enrollment> EnrollmentRepository
        //{
        //    get
        //    {

        //        if (this.enrollmentRepository == null)
        //        {
        //            this.enrollmentRepository = new GenericRepository<Enrollment>(context);
        //        }
        //        return enrollmentRepository;
        //    }
        //}

        //public GenericRepository<Grade> GradeRepository
        //{
        //    get
        //    {

        //        if (this.gradeRepository == null)
        //        {
        //            this.gradeRepository = new GenericRepository<Grade>(context);
        //        }
        //        return gradeRepository;
        //    }
        //}

        public GenericRepository<ExceptionLog> ExceptionLogRepository
        {
            get
            {
                if (this.exceptionLogRepository == null)
                {
                    this.exceptionLogRepository = new GenericRepository<ExceptionLog>(context);
                }
                return exceptionLogRepository;
            }
        }
        #endregion

        #region [Constructors]

        public UnitOfWork()
        {

        }

        #endregion

        #region [Methods]

        public void Save()
        {
            context.SaveChanges();
        }

        public async void SaveAsyc()
        {
            await context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #region [Transaction]

        public void BegginTransacation()
        {
            StartTransaction();
        }

        public void BegginTransacation(bool TransactionScopeAsyncFlowOptions)
        {
            StartTransaction(TransactionScopeAsyncFlowOptions);
        }

        public void CommitTransaction()
        {
            this.transaction.Complete();
        }

        public void RollBackTransaction()
        {
            this.transaction.Dispose();
        }

        private void StartTransaction(bool TransactionScopeAsyncFlowOptions = false)
        {
            if (TransactionScopeAsyncFlowOptions)
            {
                this.transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            }
            else
            {
                this.transaction = new TransactionScope();
            }
        }



        private void DisposeTransaction()
        {
            this.transaction.Dispose();
        }

        #endregion

        #endregion
    }
}
