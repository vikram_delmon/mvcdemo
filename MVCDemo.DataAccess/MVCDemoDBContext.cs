﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

using MVCDemo.Entity;
using MVCDemo.Helper;

namespace MVCDemo.DataAccess
{
    public class MVCDemoDBContext : DbContext
    {
        public MVCDemoDBContext() : base(busConstant.Settings.DataBase.SqlServer.Connections.ConnectionString.Default)
        {
            //Database.SetInitializer<MVCDemoDBContext>(new CreateDatabaseIfNotExists<MVCDemoDBContext>());
            //Database.SetInitializer<MVCDemoDBContext>(new DropCreateDatabaseIfModelChanges<MVCDemoDBContext>());
            //Database.SetInitializer<MVCDemoDBContext>(new DropCreateDatabaseAlways<MVCDemoDBContext>());

            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MVCDemoDBContext, MVCDemo.DataAccess.Migrations.Configuration>());
        }




        public DbSet<Code> Codes { get; set; }
        //public DbSet<CodeValue> CodeValues { get; set; }
        //public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Category> Categories { get; set; }
        //public DbSet<Course> Courses { get; set; }
        //public DbSet<Grade> Grades { get; set; }

        public DbSet<ExceptionLog> ExceptionLogs { get; set; }

        //public DbSet<NavigationMenu> NavigationMenus { get; set; }

        //public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        //{
        //    return base.Set<TEntity>();
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.HasDefaultSchema("dbo");

            base.OnModelCreating(modelBuilder);
        }
    }
}
