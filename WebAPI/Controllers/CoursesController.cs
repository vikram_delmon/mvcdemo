﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVCDemo.Entity;
using MVCDemo.DataAccess;
using MVCDemo.Helper;
using MVCDemo.DataAccess.Implementations;

namespace WebAPI.Controllers
{
    public class CoursesController : ApiController
    {
        #region [Fields]

        private UnitOfWork unitOfWork;
        #endregion

        #region [Constructors]

        public CoursesController()
        {
            this.unitOfWork = new UnitOfWork();

        }
        public CoursesController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        #endregion
        // GET api/<controller>
        public IHttpActionResult Get(int pageIndex, int pageSize)
        {
            pageIndex = pageIndex.AnyOfThis(busConstant.Numbers.Integer.Zero, busConstant.Numbers.Integer.One) ? busConstant.Numbers.Integer.Zero : pageIndex;
            pageSize = pageSize - 1;
            var courses = unitOfWork.CourseRepository.Get().Take(pageSize).Skip(pageIndex * pageSize);
            if (courses.IsNotNull() && courses.Count() > busConstant.Numbers.Integer.Zero)
            {
                return Ok(courses);
            }
            return NotFound();
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}